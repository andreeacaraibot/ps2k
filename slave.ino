/* Example sketch to control a 28BYJ-48 stepper motor with ULN2003 driver board and Arduino UNO. More info: https://www.makerguides.com */
// Include the Arduino Stepper.h library:
#include <Stepper.h>
// Define number of steps per rotation:
const int stepsPerRevolution = 2048;
// Wiring:
// Pin 8 to IN1 on the ULN2003 driver
// Pin 9 to IN2 on the ULN2003 driver
// Pin 10 to IN3 on the ULN2003 driver
// Pin 11 to IN4 on the ULN2003 driver
// Create stepper object called 'myStepper', note the pin order:
//Stepper myStepper = Stepper(stepsPerRevolution, 8, 10, 9, 11);

// Create stepper object called 'myStepper', note the pin order:
Stepper doze = Stepper(stepsPerRevolution, 13, 11, 12, 10); //IN1 IN3 IN2 IN4
Stepper cioco_1 = Stepper(stepsPerRevolution, 9, 7, 8, 6); //IN1 IN3 IN2 IN4
Stepper cioco_2 = Stepper(stepsPerRevolution, 5, 3, 4, 2); //IN1 IN3 IN2 IN4

void setup() {
  // Set the speed to 10 rpm:
  doze.setSpeed(10);
  cioco_1.setSpeed(5);
  cioco_2.setSpeed(5);
  
  // Begin Serial communication at a baud rate of 9600:
  //Serial.begin(9600);

  //pini analogici - valori de la 0 la 1023
  pinMode(A0,INPUT_PULLUP); //doze
  pinMode(A1,INPUT_PULLUP); //cioco_1
  pinMode(A2,INPUT_PULLUP); //cioco_2
  delay(2000);
}
void loop() {  //COD SLAVE!!

  if(digitalRead(A0) == LOW)
  {
    doze.step(-stepsPerRevolution/2);
    delay(500); //sta pe loc atata vreme
  
    // Jumatate de rotatie in cealalta directie:
    //Serial.println("counterclockwise");
    doze.step(stepsPerRevolution/2);
  }
  else if (digitalRead(A1) == LOW)
  {
    cioco_1.step(-stepsPerRevolution);
  }
  else if (digitalRead(A2) == LOW)
  {
    cioco_2.step(-stepsPerRevolution);
  }
}
