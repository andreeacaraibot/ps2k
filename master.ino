#include <Wire.h> 
#include <LiquidCrystal_I2C.h>

//I2C pins declaration
LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE); 

int contor_over_10 = 0; //pentru overflow
int contor_over_50 = 0;
int contor_doze = 1;
int contor_jeleu_1 = 1;
int contor_jeleu_2 = 1;
double suma_bani = 0;
int delay_ecran = 0;
int echo = 10, trig = 11;
long duration;
int distanceCM;
bool val_50 = true;
bool val_10 = true;
void setup() 
{
  lcd.begin(16,2);//Defining 16 columns and 2 rows of lcd display
  lcd.backlight();//To Power ON the back light

  pinMode(13,INPUT); //output senzor 10
  pinMode(12,INPUT); //output senzor 50

  //Senzor de distanta
  pinMode(10,INPUT); //echo
  pinMode(11,OUTPUT); //trigger

  pinMode(4,OUTPUT); //jeleu 2
  pinMode(3,OUTPUT); //jeleu 1
  pinMode(2,OUTPUT); //doze
  digitalWrite(2,HIGH);
  digitalWrite(3,HIGH);
  digitalWrite(4,HIGH);

  //Butoane
  pinMode(6,INPUT);
  pinMode(7,INPUT);
  pinMode(8,INPUT);
  pinMode(9,INPUT);


  lcd.clear();//Clean the screen
  lcd.setCursor(0,0); //Defining positon to write from first row,first column .
  lcd.print("Hello!"); //You can write 16 Characters per line .
  delay(2000);
  suma_bani = 0;
}

void loop() //cod MASTER!!
{
  val_10 &= digitalRead(13);
  val_50 &= digitalRead(12);
  
  if(delay_ecran >= 75){
  //senzor distanta
  digitalWrite(trig, LOW);
  delayMicroseconds(2);
  digitalWrite(trig, HIGH);
  delayMicroseconds(10);
  digitalWrite(trig, LOW);
  duration = pulseIn(echo, HIGH); //determine duration to calculate distance
  distanceCM = duration*0.034/2; //determina distanta in functie de viteza sunetului
  lcd.clear();

  if(distanceCM < 13) //daca se detecteaza o distanta mai mica de 13 cm =>usa inchisa
  {
    if((contor_over_10 < 10 && contor_over_50 < 10) && ((contor_doze+contor_jeleu_1+contor_jeleu_2)!=0)) //nu s-a umplut cutia de monede sau mai avem produse
    {
        //lcd.clear();
        lcd.setCursor(0,0);
        lcd.print("Intorduceti bani:");

      if(val_10 == LOW) //daca detecteaza 10 bani
      {
        contor_over_10++;
        suma_bani += (double) 0.1;
        val_10 = HIGH;
      }

      if(val_50 == LOW)
      {
        contor_over_50++;
        suma_bani += (double) 0.5;
        val_50 = HIGH;
      }

      lcd.setCursor(0,1);
      lcd.print("Suma: ");
      lcd.print(suma_bani);

        //Butoane
      int R_state=digitalRead(6);
      int B1_state=digitalRead(7);
      int B2_state=digitalRead(8);
      int B3_state=digitalRead(9);

      if(R_state==HIGH) //resetam aparatul
      {
        suma_bani = 0;
        lcd.clear();
        lcd.setCursor(0,0); 
        lcd.print("La revedere!");
        delay(1500);
      }

      else if (B1_state==HIGH) //se alege doza
      {
        if(suma_bani >= 2) //Pretul se poate modifica
        {
          lcd.clear();
          lcd.setCursor(0,0);
          lcd.print("S-a ales: jeleu2");
          digitalWrite(2, LOW);//actionare motoras doze
          contor_doze--; //decrementam numarul de doze
          delay(100);
          digitalWrite(2, HIGH);
          
          delay(1000); //cat e nevoie ca produsul sa pice
          lcd.setCursor(0,1);
          lcd.print("Ridica produsul!");
          delay(3000);
          
          suma_bani=suma_bani-2;
        }
        else
        {
          lcd.clear();
          lcd.setCursor(0,0);
          lcd.print("Sold insuficient");
          lcd.setCursor(0,1);
          lcd.print("Introduceti:");
          lcd.print(2-suma_bani); 
          delay(2000);       
        }
      }

      else if(B2_state==HIGH)
      {
        if(suma_bani >= 1.3)//Pretul se poate modifica
        {
          lcd.clear();
          lcd.setCursor(0,0);
          lcd.print("S-a ales: jeleu1");
          digitalWrite(3, LOW); //actionare motoras doze
          contor_jeleu_1--; //decrementam
          delay(100);
          digitalWrite(3, HIGH);
          
          delay(1000); //cat e nevoie ca produsul sa pice
          lcd.setCursor(0,1);
          lcd.print("Ridica produsul!");
          delay(3000);
          
          suma_bani=suma_bani-1.3;
        }
        else
        {
          lcd.clear();
          lcd.setCursor(0,0);
          lcd.print("Sold insuficient");
          lcd.setCursor(0,1);
          lcd.print("Introduceti:");
          lcd.print(1.3-suma_bani); 
          delay(2000);       
        }
      }
      else if(B3_state==HIGH)
      {
        if(suma_bani >= 0.8) //Pretul se poate modifica
        {
          lcd.clear();
          lcd.setCursor(0,0);
          lcd.print("S-a ales: doza");
          digitalWrite(4, LOW); //actionare motoras doze
          contor_jeleu_2--; //decrementam numarul de jeleuri
          digitalWrite(4, HIGH);
          delay(100);
          
          delay(1000); //cat e nevoie ca produsul sa pice
          lcd.setCursor(0,1);
          lcd.print("Ridica produsul!");
          delay(3000);
          
          suma_bani=suma_bani-0.8;
        }
        else
        {
          lcd.clear();
          lcd.setCursor(0,0);
          lcd.print("Sold insuficient");
          lcd.setCursor(0,1);
          lcd.print("Introduceti:");
          lcd.print(0.8-suma_bani); 
          delay(2000);    
        }
      }
    }

    else //cutia de bani plina sau nu mai avem produse
    {
      lcd.setCursor(0,0); 
      lcd.print("Aparatul nu"); 
      lcd.setCursor(0,1);
      lcd.print(" serveste!");
    }
  }

  else //mentenanta
  {
    suma_bani=0; 
    contor_over_10 = 0;
    contor_over_50 = 0;
    contor_doze = 1;
    contor_jeleu_1 = 1;
    contor_jeleu_2 = 1;
    lcd.setCursor(0,0);
    lcd.print("Mentenanta!");
  }
  delay_ecran = 0;
  }
  delay_ecran++;
  delay(10); //se actualizeaza ecranul la fiecare 0.75 secunde
}
