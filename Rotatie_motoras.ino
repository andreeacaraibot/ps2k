
/* Example sketch to control a 28BYJ-48 stepper motor with ULN2003 driver board and Arduino UNO. More info: https://www.makerguides.com */
// Include the Arduino Stepper.h library:
#include <Stepper.h>
// Define number of steps per rotation:
const int stepsPerRevolution = 2048;
// Wiring:
// Pin 8 to IN1 on the ULN2003 driver
// Pin 9 to IN2 on the ULN2003 driver
// Pin 10 to IN3 on the ULN2003 driver
// Pin 11 to IN4 on the ULN2003 driver
// Create stepper object called 'myStepper', note the pin order:
Stepper myStepper = Stepper(stepsPerRevolution, 8, 10, 9, 11);
void setup() {
  // Set the speed to 10 rpm:
  myStepper.setSpeed(10);
  
  // Begin Serial communication at a baud rate of 9600:
  Serial.begin(9600);
}
void loop() {
  // Pentru doze: 
  // Jumatate de rotatie intr-o directie:
  Serial.println("clockwise");
  myStepper.step(stepsPerRevolution/2);
  delay(500); //sta pe loc atata vreme
  
  // Jumatate de rotatie in cealalta directie:
  Serial.println("counterclockwise");
  myStepper.step(-stepsPerRevolution/2);
  delay(1000);

  // Pentru dulciuri: 
  // Jumatate de rotatie intr-o directie:
  Serial.println("clockwise");
  myStepper.step(stepsPerRevolution);
  delay(500); //sta pe loc atata vreme
}
