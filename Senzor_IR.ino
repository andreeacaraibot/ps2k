/* www.learningbuz.com */
/*Impport following Libraries*/
#include <Wire.h> 
#include <LiquidCrystal_I2C.h>
//I2C pins declaration
LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE); 

int contor = 0;
bool objectDetect=false;

void setup() 
{

lcd.begin(16,2);//Defining 16 columns and 2 rows of lcd display
lcd.backlight();//To Power ON the back light
//lcd.backlight();// To Power OFF the back light

pinMode(9,INPUT); //output senzor

}

void loop() 
{
int val=digitalRead(9);
if(val !=0){
  objectDetect = false; //then the object was not detected
}
else{
  objectDetect = true; // then an object was truly detected. 
  contor ++;
  
  lcd.clear();//Clean the screen
  lcd.setCursor(0,0); //Defining positon to write from first row,first column .
  lcd.print(contor); //You can write 16 Characters per line .
  delay(100);
}

}
